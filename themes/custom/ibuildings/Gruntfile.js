'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        shell: {
            bundler: {
                command: 'bundle'
            },
            bower: {
                command: 'bower install --config.interactive=false'
            },
            jekyllServe: {
              command: 'jekyll serve  --config _config-jekyll.yml'
            },
            jekyllBuild: {
                command: 'jekyll build --config _config-jekyll.yml'
            }
        },

        bower_concat: {
            all: {
                dest: 'js/_bower.js',
                exclude: [
                    'Cortana'
                ]
            }
        },


        // trying to use sasslib
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'css/app.css': 'sass/app.scss'
                }
            }
        },

        // Grunt Contrib Compass
        // https://github.com/gruntjs/grunt-contrib-compass

        compass: {
            dev: {
                options: {
                    require: ['sass-globbing'],
                    sassDir: 'sass',
                    cssDir: 'css',
                    environment: 'development',
                    outputStyle: 'expanded',
                    bundleExec: true,
                    sourcemap: true
                }
            },
            dist: {
                options: {
                    require: ['sass-globbing'],
                    environment: 'production',
                    outputStyle: 'compressed',
                    bundleExec: true,
                    sourcemap: false
                }
            }

        },

        hologram: {
            generate: {
                options: {
                    config: './hologram/hologram_config.yml'
                }
            }
        },

        // Grunt connect
        // https://github.com/gruntjs/grunt-contrib-connect
        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'design',
                    open: {
                        target: 'http://localhost:9001/_site/'
                    }
                }
            }
        },

        // Watch for changes and trigger compass and livereload
        // https://github.com/gruntjs/grunt-contrib-watch

        watch: {

            sass: {
                files: ['sass/**/*'],
                tasks: ['sass:dist','copy'],
                options: {
                    livereload: false,
                    sourceMap: true
                }
            },
            hologram: {
                files: ['sass/**/*'],
                tasks: ['hologram']
            },
            livereload: {
                options: {
                    livereload: 1337
                },
                files: [
                    'design/styleguide/*.html'
                ]
            },
            copy: {
                files:['sass/**/*'],
                task: ['copy:main']
            },
            grunticon: {
                files:['images/icons/raw/**/*'],
                task: ['grunticon:myIcons']
            }
        },

        clean: {
            default: {
                src: ['design/styleguide']
            },
            remove_info_files: {
                src: ['node_modules/**/*.info']
            }
        },

        //minimize SVG files
        svgmin: {
            options: {
                full: true,
                plugins: [
                    { cleanupIDs: false },
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            dist: {
                expand: true,
                cwd: 'images/icons/raw',
                src: ['*.svg'],
                dest: 'images/icons/min'
            }
        },

        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    cwd: 'images/icons/min',
                    src: ['*.svg', '*.png'],
                    dest: "images/icons/dist"
                }],
                options: {
                    cssprefix: ".",
                    loadersnippet: "grunticon.loader.js",
                    defaultWidth: "96px",
                    defaultHeight: "96px"
                }
            }
        },

        csscss: {
            dist: {
                src: ['design/css/app.css']
            },
            options: {
                colorize: false,
                verbose: true,
                outputJson: true,
                minMatch: 10,
                compass: true
            }
        },

        copy: {
            main: {
                files: [
                    { expand: true, src: ['css/**','js/**','images/**','fonts/**'], dest: 'design/' }
                ]
            }

        }

    });

    grunt.registerTask('icons', [
        'svgmin',
        'grunticon',
        'copy'
    ]);

    grunt.registerTask('default', [
        'sass',
        'csscss',
        'copy:main',
        //'hologram',
        'shell:jekyllBuild',
        'watch'
    ]);

    grunt.registerTask('compile', [
        'clean:default',
        'compass:dev',
        'hologram',
        'copy:main'
    ]);

    grunt.registerTask('serve', [
        'shell:jekyllServe'
    ]);

    grunt.registerTask('build', [
        'shell:jekyllBuild'
    ]);

    grunt.registerTask('install', [
        'shell:bundler',
        'shell:bower',
        'compass:dev',
        'connect',
        'csscss',
        'copy:main',
        'hologram',
        'svgmin',
        'grunticon',
        'clean:remove_info_files'
    ]);
};

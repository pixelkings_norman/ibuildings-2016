(function($) {

    // navigation
    var navbanner = $('.navbanner');

    $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 10) {
            navbanner.addClass("navbanner--scrolled");
        } else {
            navbanner.removeClass("navbanner--scrolled");
        }
    });

    $('.shownext').on("click", function (e) {
        $(this).next().addClass('show');
        e.preventDefault();
    });

    if ($(window).scrollTop() > 10) {
        navbanner.addClass("navbanner--scrolled");
    }

})(jQuery);

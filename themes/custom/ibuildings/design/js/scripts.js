/*
 * initiate isotope
 */

jQuery( document ).ready( function( $ ) {

    // start hero slider if !mobile

    if ($('.lap-and-up').is(':visible')) {

        $('.slider--hero').slick({
            dots: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider--hero--logos',
            lazyLoad: 'progressive',
            autoplay: true,
            autoplaySpeed: 10000
        });

        $('.slider--hero--logos').slick({
            dots: false,
            speed: 500,
            asNavFor: '.slider--hero',
            centerMode: true,
            focusOnSelect: true,
            variableWidth: true,
            lazyLoad: 'progressive'
        });

    }




    // set starting panel full height on mobile to create some sort of splashscreen

    if ($('.palm').is(':visible')) {
        var totalHeight = $(window).height();
        $('.mobile-start').height(totalHeight)-196;  // remove padding
    }



    // toggle menu on mobile

    $('#toggle-menu').on('click', function () {
        $('html').toggleClass('togglemenu--active');
    });








    // typewriter effect on homepage

    if ($('.desk').is(':visible')) {
        $(".typewriter").typed({
            strings: ['Ibuildings levert het technisch fundament<br />om sneller en effectiever te communiceren.<br /> ^500 Op tijd. ^700 Binnen budget. '],
            showCursor: false,
            callback: function() {
                $('.cta-slider').css('opacity',1);
            }
        });
    } else {
        $(".typewriter").append('Ibuildings levert het technisch fundament<br />om sneller en effectiever te communiceren.<br /> Op tijd. Binnen budget.');
    }




    // technology slider

    $('.webtechnologies').slick({
        dots: true,
        infinite: true,
        speed: 300,
        centerMode: true,
        variableWidth: true,
    });





    //  toggle news panel homepage

    $('.newsslider__toggle').on('click', function () {
        $(this).parent().toggleClass('newsslider__container--closed');

    });




    //  btn mouseover

    $('.refindex').mouseenter(function () {
        $(this).find('.refindex__caption').velocity({
            paddingBottom: 90
        },{
            duration: 25,
            delay: 0
        });

        $(this).find('.btn').velocity({
            opacity:1
        },{
            duration: 25,
            delay: 100
        });
    }).mouseleave(function () {
        $(this).find('.refindex__caption').velocity({
            paddingBottom: 24,
            delay: 100
        });
        $(this).find('.btn').velocity({
            opacity:0
        },{
            duration: 25,
            delay: 0
        });
    });



    // viewport detection to trigger animation on view list page.

    function onScrollInit( items, trigger ) {
        items.each( function() {
            var osElement = $(this),
                osAnimationClass = osElement.attr('data-os-animation'),
                osAnimationDelay = osElement.attr('data-os-animation-delay');

            osElement.css({
                '-webkit-animation-delay':  osAnimationDelay,
                '-moz-animation-delay':     osAnimationDelay,
                'animation-delay':          osAnimationDelay
            });

            var osTrigger = ( trigger ) ? trigger : osElement;

            osTrigger.waypoint(function() {
                osElement.addClass('animated').addClass(osAnimationClass);
            },{
                triggerOnce: true,
                offset: '90%'
            });
        });
    }

    onScrollInit( $('.os-animation') );






    $('.refindex').mouseenter(function () {
        $(this).find('.refindex__caption').velocity({
            paddingBottom: 90
        },{
            duration: 25,
            delay: 0
        });

        $(this).find('.btn').velocity({
            opacity:1
        },{
            duration: 25,
            delay: 100
        });
    }).mouseleave(function () {
        $(this).find('.refindex__caption').velocity({
            paddingBottom: 24,
            delay: 100
        });
        $(this).find('.btn').velocity({
            opacity:0
        },{
            duration: 25,
            delay: 0
        });
    });




});


$(document).scroll(function(){

    var windowsize = $(window).width();

    //console.log(windowsize);

    if ( windowsize > 665 ) {

        $('.mainnav__l2').addClass('hide');
        $('.menu--flyout').removeClass('show-flyout');

        var scrollA = $('body').scrollTop();

        setTimeout(function () {

            if (scrollA == $('body').scrollTop()) {
                $('.mainnav__l2').removeClass("hide");
            }

        }, 200);

    }

});
